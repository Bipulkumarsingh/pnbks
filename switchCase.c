#include<stdio.h>

int main()
{
    //Program to find if character is vowel.
    char ch;
    printf("This is a program to find if your given character is a vowel.\n\nEnter your character: ");
    scanf("%c", &ch);
    switch (ch) {
	case 'A':
	case 'E':
	case 'I':
	case 'O':
	case 'U':
	case 'a':
	case 'e':
	case 'i':
	case 'o':
	case 'u':
	printf("The character %c is a vowel", ch);
	break;
    default:
    printf("The character %c is constant", ch);
	}
    return 0;
}